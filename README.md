## INSTALLATION

### Pré-requis

L'installation de docker et docker-compose est pré requise.

### Procédure d'installation

Dans la racine du dossier executer les commandes ci-dessous.

    docker-compose build --no-cache

    docker-compose up -d

Allez sur phpmyadmin à l'adresse http://localhost:82 puis créer la base de données au nom de "hooly" avec les identifiants ci-dessous.


    Username: root
    Password: dXz0lpJEMCUUvgJY


Allez dans le conteneur PHP et executez les commandes ci-dessous.
    
    docker-compose exec php bash

        -> composer install
        -> php bin/console doctrine:migrations:migrate


Allez dans le dossier /angular/front à la racine puis executez les commande ci-dessous.

    npm install
    ng serve

L'application devrait etre dorénavant disponible à l'adresse ci-dessous.

    http://localhost:4000