<?php

namespace App\Controller\Booking;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;

use App\Service\Location\LocationService;
use App\Service\Booking\BookingService;

class BookingController extends AbstractController
{
     /**
     * @Route("/api/v1/location/{id}/booking", methods={"POST"})
     */
    public function booking($id, 
                            SerializerInterface $serializer, 
                            LocationService $locationService, 
                            BookingService $bookingService, 
                            Request $request)
    {

        $location = $locationService->getById($id);

        $data = $serializer->serialize($bookingService->newBooking($location, $request->get('date')), 'json', SerializationContext::create());

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
    /**
    * @Route("/api/v1/booking/list", methods={"GET"})
    */
    public function list(SerializerInterface $serializer,
                            BookingService $bookingService, 
                            Request $request,
                            Security $security)
    {

       $data = $serializer->serialize($bookingService->list($security->getUser()), 'json', SerializationContext::create());

       $response = new Response($data);
       $response->headers->set('Content-Type', 'application/json');

       return $response;

    }
}
