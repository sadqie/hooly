<?php

namespace App\Controller\Location;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;

use App\Service\Location\LocationAvailabilityService;

class AvailabilityController extends AbstractController
{
     /**
     * @Route("/api/v1/location/availability", methods={"GET"})
     */
    public function availability(SerializerInterface $serializer, 
                                    LocationAvailabilityService $locationAvailabilityService, 
                                    Request $request)
    {
        
        $locations = $locationAvailabilityService->checkAvailabilities($request->get('date'));
        
        if(!$locations) {
            throw new NotFoundHttpException('No location found on this date');
        }

        $data = $serializer->serialize($locations, 'json', SerializationContext::create());

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
}
