<?php

namespace App\Controller\Authentication;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
Use App\Service\User\UserService;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;

use App\Entity\User;

class AuthenticationController extends AbstractController
{

	public function index(	Security $security, 
                            EntityManagerInterface $em,
							JWTEncoderInterface $jwtEncoder,
							Request $request) {

		
	}

     /**
     * @Route("/api/register", methods={"POST"})
     */
	public function createUser(SerializerInterface $serializer,
								UserService $userService,
								Request $request) {
		
		$data = json_decode($request->getContent(), true);

		if(!isset($data['username']) || !isset($data['password']) || !isset($data['email'])) {
			throw new BadRequestHttpException('Username, password and email must be sent');
		}

		$user = $userService->newUser($data['username'], $data['password'], $data['email']);
        $data = $serializer->serialize($user, 'json', SerializationContext::create());

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
	}
}