<?php

namespace App\Service\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use App\Service\Location\LocationAvailabilityService;

class UserService
{

    public function __construct(private EntityManagerInterface $em,
                                private UserPasswordHasherInterface $passwordHasher) {
    }

    public function getRepository($entity = User::class)
    {
        return $this->em->getRepository($entity);
    }
    public function newUser($username, $password, $email)
    {
        if($this->getByUsername($username)) {
			throw new BadRequestHttpException('Username already exists');
        }

        if($this->getByEmail($email)) {
			throw new BadRequestHttpException('Email already exists');
        }


		$user = new User();
		$user->setUsername($username);

		$plaintextPassword = $password;

		$hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $plaintextPassword
        );
		
        $user->setPassword($hashedPassword);

		$user->setEmail($email);

		$this->em->persist($user);
		$this->em->flush();

        return $user;
    }
    public function getByUsername($username)
    {
        $user = $this->getRepository()->findOneBy(array("username" => $username));

        return $user;
    }
    public function getByEmail($email)
    {
        $user = $this->getRepository()->findOneBy(array("email" => $email));

        return $user;
    }

}
