<?php

namespace App\Service\Location;

use App\Entity\Location;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LocationService
{

    public function __construct(private EntityManagerInterface $em) {
    }

    public function getRepository($entity = Location::class)
    {
        return $this->em->getRepository($entity);
    }
    public function getById($id)
    {
        $location = $this->getRepository()->find($id);

        if(!$location) {
            throw new NotFoundHttpException('No location found');
        }

        return $location;
    }
}
