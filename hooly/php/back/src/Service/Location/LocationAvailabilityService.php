<?php

namespace App\Service\Location;

use App\Entity\Location;
use Doctrine\ORM\EntityManagerInterface;

use App\Traits\BookingDateTrait;

class LocationAvailabilityService
{
    use BookingDateTrait;

    public function __construct(private EntityManagerInterface $em) {
    }

    public function getRepository($entity = Location::class)
    {
        return $this->em->getRepository($entity);
    }

    public function checkAvailabilities($date)
    {
        $date = $this->controlDate(date: $date);
        return $this->checkIfLocationFound($this->getRepository()->checkAvailabilities(date: $date));
    }
    
    public function checkAvailability($location, $date)
    {
        $date = $this->controlDate(date: $date);
        return $this->checkIfLocationFound($this->getRepository()->checkAvailabilities(date: $date, id: $location->getId()));
    }
}
