<?php

namespace App\Service\Booking;

use App\Entity\Booking;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

use App\Service\Location\LocationAvailabilityService;

class BookingService
{

    public function __construct(private EntityManagerInterface $em,
                                private LocationAvailabilityService $locationAvailabilityService,
                                private Security $security) {
    }

    public function getRepository($entity = Booking::class)
    {
        return $this->em->getRepository($entity);
    }
    public function newBooking($location, $date)
    {
        $booking = new Booking();
        $booking->setLocation($this->locationAvailabilityService->checkAvailability(location: $location, date: $date));
        $booking->setDate(new \DateTime($date));
        $booking->setUser($this->security->getUser());

        $this->em->persist($booking);
        $this->em->flush();

        return $booking;
    }
    public function list($user)
    {
        $bookings = $this->getRepository()->findBy(array("user" => $user->getId()));

        return $bookings;
    }
}
