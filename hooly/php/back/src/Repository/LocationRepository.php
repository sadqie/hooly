<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Location;

class LocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Location::class);
    }

    public function checkAvailabilities($date, $id = null)
    {

        $query = $this->createQueryBuilder('l')
                    ->innerJoin('l.locationAvailability', 'la')
                    ->leftJoin('App\Entity\Booking', 'b', 'WITH', 'b.location = l.id AND b.date = :date OR (WEEK(b.date) = WEEK(:date)) ')
                    ->where('b.id is NULL')
                    ->andWhere('(   (DAYOFWEEK(:date) = 2 AND la.monday = 1)
                                OR  (DAYOFWEEK(:date) = 3 AND la.tuesday = 1)  
                                OR  (DAYOFWEEK(:date) = 4 AND la.wednesday = 1) 
                                OR  (DAYOFWEEK(:date) = 5 AND la.thursday = 1) 
                                OR  (DAYOFWEEK(:date) = 6 AND la.friday = 1) 
                                OR  (DAYOFWEEK(:date) = 7 AND la.saturday = 1) 
                                OR  (DAYOFWEEK(:date) = 1 AND la.sunday = 1)      )')
                    ->setParameter('date', $date);
        if($id) {
            $query->andWhere('l.id = :id')
            ->setParameter('id', $id);
            return $query->getQuery()->getOneOrNullResult();;
        }

        return $query->getQuery()->getScalarResult();;
    }
}
