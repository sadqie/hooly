<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table()
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class LocationAvailability
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose
     */
    private ?bool $monday = null;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose
     */
    private ?bool $tuesday = null;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose
     */
    private ?bool $wednesday = null;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose
     */
    private ?bool $thursday = null;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose
     */
    private ?bool $friday = null;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose
     */
    private ?bool $saturday = null;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose
     */
    private ?bool $sunday = null;


    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMonday(): ?bool
    {
        return $this->monday;
    }
    public function setMonday(bool $name): void
    {
        $this->monday = $monday;
    }
    
    public function getTuesday(): ?bool
    {
        return $this->tuesday;
    }
    public function setTuesday(bool $tuesday): void
    {
        $this->tuesday = $tuesday;
    }
    
    public function getWednesday(): ?bool
    {
        return $this->wednesday;
    }
    public function setWednesday(bool $wednesday): void
    {
        $this->wednesday = $wednesday;
    }
    
    public function getThursday(): ?bool
    {
        return $this->thursday;
    }
    public function setThursday(bool $thursday): void
    {
        $this->thursday = $thursday;
    }
    
    public function getFriday(): ?bool
    {
        return $this->friday;
    }
    public function setFriday(bool $friday): void
    {
        $this->friday = $friday;
    }
    
    public function getSaturday(): ?bool
    {
        return $this->saturday;
    }
    public function setSaturday(bool $saturday): void
    {
        $this->saturday = $saturday;
    }
    
    public function getSunday(): ?bool
    {
        return $this->sunday;
    }
    public function setSunday(bool $sunday): void
    {
        $this->sunday = $sunday;
    }

}
