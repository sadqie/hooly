<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 * @ORM\Table()
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class Location
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text")
     * @Serializer\Expose
     */
    private ?string $name = null;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\LocationAvailability")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Expose
     */
    private ?LocationAvailability $locationAvailability = null;


    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLocationAvailability(): ?LocationAvailability
    {
        return $this->locationAvailability;
    }
    public function setLocationAvailability(LocationAvailability $locationAvailability): void
    {
        $this->locationAvailability = $locationAvailability;
    }
}
