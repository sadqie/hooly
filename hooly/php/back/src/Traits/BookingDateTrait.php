<?php

namespace App\Traits;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait BookingDateTrait
{
    private function controlDate($date) {

        $date = \DateTime::createFromFormat('Y-m-d', $date) ;

        if ($date === false) {
            throw new BadRequestHttpException('The date parameter must be set and in YYYY-MM-DD format');
        }

        $now = new \DateTime();
    
        if($date < $now) {
            throw new BadRequestHttpException('You cannot reserve a location for the same day or a past date');
        }

        return $date->format('Y-m-d');
    }
    private function checkIfLocationFound($data) {
        if(!$data) {
            throw new BadRequestHttpException('No location found on this date, you may already have a location this week');
        } else {
            return $data;
        }
    }
}
