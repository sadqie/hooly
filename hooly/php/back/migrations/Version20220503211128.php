<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220503211128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, location_id INT NOT NULL, user_id INT NOT NULL, date DATE NOT NULL, INDEX IDX_E00CEDDE64D218E (location_id), INDEX IDX_E00CEDDEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, location_availability_id INT NOT NULL, name LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_5E9E89CBD1370922 (location_availability_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location_availability (id INT AUTO_INCREMENT NOT NULL, monday TINYINT(1) NOT NULL, tuesday TINYINT(1) NOT NULL, wednesday TINYINT(1) NOT NULL, thursday TINYINT(1) NOT NULL, friday TINYINT(1) NOT NULL, saturday TINYINT(1) NOT NULL, sunday TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(25) NOT NULL, password VARCHAR(64) NOT NULL, email VARCHAR(254) NOT NULL, is_active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBD1370922 FOREIGN KEY (location_availability_id) REFERENCES location_availability (id)');
        
        $this->addSql("INSERT INTO `location_availability` (`id`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`, `sunday`) VALUES (NULL, '1', '1', '1', '1', '1', '1', '1'), (NULL, '1', '1', '1', '1', '1', '1', '1'), (NULL, '1', '1', '1', '1', '1', '1', '1'), (NULL, '1', '1', '1', '1', '1', '1', '1'), (NULL, '1', '1', '1', '1', '0', '1', '1'), (NULL, '1', '1', '1', '1', '1', '1', '1'), (NULL, '1', '1', '1', '1', '1', '1', '1');");
        $this->addSql("INSERT INTO `location` (`id`, `location_availability_id`, `name`) VALUES (NULL, '1', 'Location 1'), (NULL, '2', 'Location 2'), (NULL, '3', 'Location 3'), (NULL, '4', 'Location 4'), (NULL, '5', 'Location 5'), (NULL, '6', 'Location 6'), (NULL, '7', 'Location 7');");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE64D218E');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CBD1370922');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEA76ED395');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE location_availability');
        $this->addSql('DROP TABLE user');
    }
}
