import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { FlashMessageService } from '@shared/modules/alert/flash-message.service';
import { UserRepository } from '@shared/repositories/user.repository';
import { AuthGuardService } from '@shared/services/auth-guard.service';
import { AuthService } from '@shared/services/auth.service';

import { Observable, Subject } from 'rxjs';
import { filter, map, mapTo, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  loading: boolean = false;
  token: string = null;
  registerActive: boolean = false;

  logout$: Observable<boolean> = this.route.queryParams.pipe(
    filter((val) => val.logout !== "true"),
    mapTo(true),
    takeUntil(this.destroy$)
  );

  signInForm: FormGroup = new FormGroup({
    'username': new FormControl(null, Validators.required),
    'password': new FormControl(null, Validators.required)
  });

  registerForm: FormGroup = new FormGroup({
    'username': new FormControl(null, Validators.required),
    'password': new FormControl(null, Validators.required),
    'email': new FormControl(null, Validators.required)
  });

  constructor(
    private authService: AuthService,
    private flashMessageService: FlashMessageService,
    private route: ActivatedRoute,
    private authGuard: AuthGuardService,
    private userRepository: UserRepository,
    private router: Router) {
    this.route.queryParams.pipe(map((params: Params) => params.token), tap((token: string) => this.token = token), takeUntil(this.destroy$)).subscribe();
  }

  ngOnInit() {
    //if param logout exist and equal true : sign out
    this.logout$.pipe(takeUntil(this.destroy$)).subscribe();

    if (this.authGuard.isAuthenticated()) {
      this.router.navigate([this.authService.getHome()]);
    }
  }


  signIn() {

    const username = this.signInForm.value['username'];
    const password = this.signInForm.value['password'];
    this.loading = true;
    this.authService.signIn({ 'username': username, 'password': password }).then(
      () => {
        this.flashMessageService.showMessage('success', 'Successfuly authenticated');
        this.router.navigate([this.authService.getHome()]);
      }
    ).catch((err) => {
    }).finally(() => this.loading = false);
  }

  onSignOut() {
    this.authService.signOut();
  }

  goToRegister() {
    this.registerActive = true;
  }

  goToLogin() {
    this.registerActive = false;
  }

  register() {
    if(!this.registerForm.value.username || !this.registerForm.value.password || !this.registerForm.value.email ) {
      return;
    }
    let username = this.registerForm.value.username;
    let password = this.registerForm.value.password;
    let email = this.registerForm.value.email;

    this.userRepository.create(username, password, email).pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.flashMessageService.showMessage('success', 'Register successfully');
      this.goToLogin();
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
