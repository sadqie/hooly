import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthGuardService } from '@shared/services/auth-guard.service';
import { AuthService } from '@shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private authService: AuthService,
              private  authGuardService: AuthGuardService) {

  }

  isAuthenticated(){
    return this.authGuardService.isAuthenticated();
  }
  signOut() {
    this.authService.signOut();
  }
}
