import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { AuthGuardService } from '@shared/services/auth-guard.service';

const appRoutes: Routes = [
  {
    path: '',
    component: AuthComponent
  },
  {
    path: 'booking',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./modules/booking/booking.module').then(m => m.BookingModule),
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      onSameUrlNavigation: 'reload',
      scrollPositionRestoration: 'enabled'
    })
  ],
  providers: [
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }