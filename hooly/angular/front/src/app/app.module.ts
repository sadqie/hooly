import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './components/app.component';
import { AuthComponent } from './components/auth/auth.component';

import { AlertModule } from 'src/app/shared/modules/alert/alert.module';
import { AuthService } from '@shared/services/auth.service';
import { ApiInterceptor } from './shared/http-interceptor/api-interceptor';
import { FormatInterceptor } from './shared/http-interceptor/format-interceptor';
import { ErrorInterceptor } from './shared/http-interceptor/error-interceptor';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from '@shared/modules/material/material.module';
import { AuthGuardService } from '@shared/services/auth-guard.service';
import { BookingRepository } from '@shared/repositories/booking.repository';
import { LocationRepository} from '@shared/repositories/location.repository';
import { UserRepository } from '@shared/repositories/user.repository';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    AppRoutingModule,
    AlertModule,
    MaterialModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: FormatInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AuthService,
    AuthGuardService,
    BookingRepository,
    LocationRepository,
    UserRepository
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor() {
    
  }
}
