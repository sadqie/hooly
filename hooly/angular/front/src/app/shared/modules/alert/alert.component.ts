import { Component, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlashMessageService } from './flash-message.service';

@Component({
  selector: 'app-alert',
  template: '',
  styleUrls: ['./alert.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AlertComponent implements AfterViewInit{

  private destroy$: Subject<boolean> = new Subject<boolean>();

  snackBarConfig: MatSnackBarConfig = new MatSnackBarConfig();
  messageQueue: any[];

  constructor(
    private flashMessageService: FlashMessageService,
    private snackBar: MatSnackBar) {
    this.snackBarConfig.duration = 5000;
    this.messageQueue = this.flashMessageService.getMessages();
    this.flashMessageService.liveMessage.pipe(takeUntil(this.destroy$)).subscribe((data) => this.open(data));
  }

  ngAfterViewInit(){
    setTimeout(() => this.openSnackBars(), 500);
    ;
  }

  openSnackBars() {
    if (this.messageQueue.length) {
      let message = this.messageQueue.shift();
      let ref = this.open(message);
      if(message.action && message.action.callback){
        ref.onAction().pipe(takeUntil(this.destroy$)).subscribe(() => message.action.callback.call());
      }
      if(this.messageQueue.length){
        ref.afterDismissed().pipe(takeUntil(this.destroy$)).subscribe(() => this.openSnackBars());
      }
    }
  }

  open(message){
    if(message){
      this.snackBarConfig.panelClass = message.type;
      this.snackBarConfig.data = message;
      return this.snackBar.open(message.content, message.action ? message.action.name : message.type.charAt(0).toUpperCase() + message.type.slice(1), this.snackBarConfig);
    }
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
