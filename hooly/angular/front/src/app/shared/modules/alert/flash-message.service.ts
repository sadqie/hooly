import {Injectable} from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn : 'root'
})
export class FlashMessageService {

  messages: any[] = [];

  public liveMessage: Subject<any> = new Subject();

  constructor() {
  }

  showMessage(type, content, extra?){
    this.liveMessage.next({type: type, content: content, extra: extra});
  }

  addMessage(type, content, extra?){
    this.messages.push({type: type, content: content, extra: extra});
  }

  getMessages(type?){
    if(type){
      let remaining = [];
      let matching = [];
      this.messages.forEach((elt) => {
          if(elt.type == type)
            matching.push(elt);
          else
            remaining.push(elt);
      });
      this.messages = remaining;
      return matching;
    }else{
      let messages = this.messages.slice();
      this.messages = [];
      return messages;
    }
  }

  debugMessages(){
    return this.messages;
  }

}
