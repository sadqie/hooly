import { NgModule } from '@angular/core';
import { MaterialModule } from '../material/material.module';
import { AlertComponent } from './alert.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AlertComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  entryComponents:[
    AlertComponent
  ],
  exports: [
    AlertComponent
  ]
})


export class AlertModule {
  constructor() {

  }
}
