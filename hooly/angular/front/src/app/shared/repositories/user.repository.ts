import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class UserRepository  {

  constructor( private http: HttpClient) {
  }
  create(username, password, email) {
    return this.http.post('register', { username: username, password:password, email:email });
  }
}