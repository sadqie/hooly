import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BookingRepository  {

  constructor( private http: HttpClient) {
  }
  all() {
    return this.http.get('v1/booking/list');
  }
}