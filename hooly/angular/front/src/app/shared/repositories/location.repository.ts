import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class LocationRepository  {

  constructor( private http: HttpClient) {
  }
  findAvailability(date) {
    let queryParams = new HttpParams();
    queryParams = queryParams.append("date",date);
    return this.http.get('v1/location/availability', { params: queryParams });
  }
  book(date, id) {
    return this.http.post('v1/location/' + id + '/booking?date=' + date, {});
  }
}