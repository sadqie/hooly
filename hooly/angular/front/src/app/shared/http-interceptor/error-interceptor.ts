
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { FlashMessageService } from '../modules/alert/flash-message.service';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, 
              private router: Router, 
              private flashMessageService: FlashMessageService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      catchError((error, caught) => {
        return this.handleError(req, error, caught, next);
      })
    );
  }

  handleError(req, error, caught, next): Observable<any> {
    if(error.status == 401){
      return this.handle401(req, error, caught, next);
    }

    if (req.headers.get('x-error-handling') != 'local') {
      switch (error.status) {
        case 401: return this.handle401(req, error, caught, next);
        case 400: this.handle400(req, error); return throwError('error');
        case 0: this.handleOther(req, error); return throwError('error');
      }

    }

    return throwError(error);
  }

  handle401(req, error, caught, next) {
    this.flashMessageService.showMessage('error', error.error.message, error.message);
    this.authService.signOut();
    if(this.shouldBeAuthenticated(req)){
      this.router.navigate(['/']);
    }

    return throwError(error);
  }

  handle400(req, error) {
    this.flashMessageService.showMessage('error', error.error.detail, error.message);
  }

  handleOther(req, error) {
    this.flashMessageService.showMessage('error', 'API server seems to be unreachable', error.message);
    this.router.navigate(['/error']);
  }

  shouldBeAuthenticated(req) {
    if ( !req.url.includes('login_check')) {
      return true;
    }
    return false;
  }

  addAuthenticationToken(request) {
    if (!this.authService.getToken()) {
      return request;
    }

    // We clone the request, because the original request is immutable
    return request.clone({
      setHeaders: this.authService.getHeaders()
    });
  }
}