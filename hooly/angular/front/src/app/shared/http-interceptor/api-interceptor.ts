
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { config } from '../../../assets/config';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  
  constructor(private authService: AuthService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Get the auth token from the service.
    const apiHeaders = this.authService.getHeaders();
    if(!this.shouldBeAuthenticated(req)){
      delete apiHeaders['Authorization'];
    }
    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({ setHeaders: apiHeaders, url : config.apiBaseUrl + config.apiPrefix + '/' + req.url  });
    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }

  handleError(req, error, caught, next): Observable<any> {
    if (error.status == 401 && this.shouldBeAuthenticated(req)) {
      this.router.navigate(['/']);
    }else{
      return caught;
    }
  }

  shouldBeAuthenticated(req) {
    if (!req.url.includes('login_check') && !req.url.includes('register')) {
      return true;
    }
    return false;
  }

  addAuthenticationToken(request) {
    if (!this.authService.getToken()) {
      return request;
    }

    // We clone the request, because the original request is immutable
    return request.clone({
      setHeaders: this.authService.getHeaders()
    });
  }
}