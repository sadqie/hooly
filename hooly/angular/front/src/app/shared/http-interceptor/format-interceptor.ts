
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable()
export class FormatInterceptor implements HttpInterceptor {

  constructor() { }

  
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let formattedReq = req;

    const apiHeaders = {};
    if(!req.headers.has('content-type'))
      apiHeaders["Content-Type"] = "application/json";

    if(req.method == 'PUT' || req.method == 'PATCH' || req.method == 'POST'){
      if(req.body instanceof FormData){
          delete apiHeaders["Content-Type"];
      }
      
      formattedReq = req.clone({  body : req.body, setHeaders : apiHeaders });
    }

    return next.handle(formattedReq).pipe(
      map((res => this.formatResponse(formattedReq, res)))
    );
  }

  formatResponse(req, res) {
    return res;
  }
}