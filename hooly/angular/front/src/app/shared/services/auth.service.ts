import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlashMessageService } from '../modules/alert/flash-message.service';

@Injectable()
export class AuthService {

  destroy$: Subject<boolean> = new Subject<boolean>();

  // http options used for making API calls
  private httpOptions: any;

  // the username of the logged in user
  public username: string;

  // the id of the logged in user
  public userId: number;

  // error messages received from the login attempt
  public errors: any = [];


  public refreshingToken: boolean = false;
  public requestsPendingRefreshToken: any[] = [];
  public refreshTokenNotificationDelay: number = 90;

  public authSubscription: Subscription;


  public signoutNotificationSubscription: Subscription;

  constructor(private http: HttpClient,
    private flashMessageService: FlashMessageService) {
    this.getToken();
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
  }

  getHeaders() {
    return {
      'Authorization': 'Bearer ' + this.getToken()
    };
  }

  getAuthentifiedHttpHeaders() {
    return new HttpHeaders(this.getHeaders());
  }

  signIn(user) {
    return new Promise<void>((resolve, reject) => {
      const auth = this.http.post('login_check', JSON.stringify(user), Object.assign({}, this.httpOptions, { observe: 'response' }));
      let authSubscription = auth.pipe(takeUntil(this.destroy$)).subscribe(
        (response: any) => {
          let data = response.body;
          let headers = response.headers as HttpHeaders;
          this.updateData(data['token']);
          resolve();
        },
        err => {
          this.errors = err['message'];
          reject(err);
        }
      );
    });

  }
  private updateData(token) {
    localStorage.setItem('app-user-token', token);
    this.errors = [];
    this.username = this.getUsername();
  }

  public getUsername() {
    const token = localStorage.getItem('app-user-token');
    
    const token_parts = token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.username = token_decoded.username;

    return this.username;
  }

  public getToken() {
    return localStorage.getItem('app-user-token');
  }

  getHome() {
    if (!this.getToken()) {
      return false;
    } else {
      return 'booking';
    }
    
      
  }

  signOut() {
    this.username = null;
    this.userId = null;
    localStorage.removeItem('app-user-token');
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
