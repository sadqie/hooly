import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookingRepository } from '@shared/repositories/booking.repository';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-booking-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  destroy$: Subject<boolean> = new Subject<boolean>();

  list: any = [];

  constructor(private bookingRepository: BookingRepository) { 
  }

  ngOnInit(): void {
    this.bookingRepository.all().pipe(takeUntil(this.destroy$)).subscribe((data) => {
      this.list = data;
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
