import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LocationRepository } from '@shared/repositories/location.repository';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { FlashMessageService } from '@shared/modules/alert/flash-message.service';
import { AuthService } from '@shared/services/auth.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {


  destroy$: Subject<boolean> = new Subject<boolean>();

  searchForm: FormGroup = new FormGroup({
    'date': new FormControl(null, Validators.required)
  });

  availabilities: any = null;
  date: any = null;

  constructor(private locationRepository: LocationRepository,
              private flashMessageService: FlashMessageService,
              private router: Router,
              private authService: AuthService) { }

  ngOnInit(): void {
  }

  search() {

    if(!this.searchForm.value.date) {
      return;
    }
    this.date = moment(this.searchForm.value.date)?.format('YYYY-MM-DD');
    this.locationRepository.findAvailability(this.date).pipe(takeUntil(this.destroy$)).subscribe((data) => {
      this.availabilities = data;
    });
  }
  unsetAvailabilities() {
    this.availabilities = null;
  }
  book(id) {
    this.locationRepository.book(this.date, id).pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.flashMessageService.showMessage('success', 'Successfuly booking');
      this.router.navigate([this.authService.getHome()]);
    });
  }
  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
