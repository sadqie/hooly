import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ListComponent } from './components/list/list.component';
import { NewComponent } from './components/new/new.component';
import { MaterialModule } from '@shared/modules/material/material.module';
import { BookingRoutingModule } from './booking-routing.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    BookingRoutingModule

  ],
  exports: [
    RouterModule,
  ],
  entryComponents: [
  ],
  declarations: [
    ListComponent,
    NewComponent
  ]
})
export class BookingModule { }
